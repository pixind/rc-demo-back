class ComercialController < ApplicationController


  def neumaticos
    @filtros_form = FiltrosForm.new
    @filtros ||= Filtros.new

    if request.post?

      @filtros.marca = params[:filtros][:marca]
      @filtros.anchura = params[:filtros][:anchura]
      @filtros.altura = params[:filtros][:altura]
      @filtros.diametro = params[:filtros][:diametro]
      @filtros.carga = params[:filtros][:carga]
      @filtros.temporada = params[:filtros][:temporada]
      @filtros.entrega = params[:filtros][:entrega]
      @filtros.velocidad  = params[:filtros][:velocidad]

      @neumaticos = Tyre.activos.coche.ordenados.filtrados(@filtros).limit(50)

    else
      @neumaticos = []
    end

  end


  def neumatico
    @neumatico = Tyre.find(params[:id])
  end

end
