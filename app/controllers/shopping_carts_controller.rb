class ShoppingCartsController < ApplicationController

  def create
    @neumatico = Tyre.find(params[:id])
    @shopping_cart.add(@neumatico, @neumatico.pvp_t1_iva)
    redirect_to "/carrito"
  end

  def show

  end

  def modify
    @item = ShoppingCartItem.find(params[:id])
    @item.quantity = params[:qty]
    @item.save!
    redirect_to "/carrito"
  end

  def remove
    @item = ShoppingCartItem.find(params[:id])
    @item.destroy!
    if @shopping_cart.total_unique_items >= 1
      redirect_to "/carrito"
    else
      redirect_to "/taller/neumaticos"
    end

  end

  def clear
    @shopping_cart.clear
    redirect_to "/taller/neumaticos"
  end

  def save_as_bugdet
    @presupuesto = Presupuesto.create(cliente_id: @cliente.id)
    @presupuesto.save!

    @shopping_cart.shopping_cart_items.each do |item|
      presupuesto_item = PresupuestoItem.create(presupuesto_id: @presupuesto.id, quantity: item.quantity, item_id: item.item_id, price: item.price)
      presupuesto_item.save!
    end

    @shopping_cart.clear

    redirect_to "/carrito"

  end

  def destroy_budget
    presupuesto = Presupuesto.find(params[:id])
    presupuesto.destroy!
    redirect_to "/carrito"
  end

  def update_sups
    
  end

end