class AlmacenController < ApplicationController

  def inicio
    @pedidos = Pedido.all.limit(33)
  end

  def pendientes
    @pedidos = Pedido.all.limit(3)

  end

  def enviados
    @pedidos = Pedido.all.limit(11)
  end

end
