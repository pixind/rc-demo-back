class ClientesController < ApplicationController
  before_action :set_cliente, only: [:show, :edit, :update, :destroy]

  def index
    @clientes = Cliente.all.limit(30)
  end

  def show
  end

  def create
    @cliente = Cliente.new(cliente_params)

    respond_to do |format|
      if @cliente.save
        format.html { redirect_to @cliente, notice: 'Cliente was successfully created.' }
        format.json { render :show, status: :created, location: @cliente }
      else
        format.html { render :new }
        format.json { render json: @cliente.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @cliente.update(cliente_params)
        format.html { redirect_to @cliente, notice: 'Cliente was successfully updated.' }
        format.json { render :show, status: :ok, location: @cliente }
      else
        format.html { render :edit }
        format.json { render json: @cliente.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @cliente.destroy
    respond_to do |format|
      format.html { redirect_to clientes_url, notice: 'Cliente was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def buscar
    sugerencias = Cliente.select(:nombre_empresa).where("nombre_empresa like ?", "%#{params[:term]}%")
    render json: sugerencias
  end

  def seleccionar
    if request.post?
      cliente = Cliente.where("nombre_empresa like ?", "%#{params[:nombre]}%").first
      path = "/carrito"
    end
    if request.get?
      cliente = Cliente.find(params[:id])
      path = "/carrito" if params[:proc] == "cart"
      path = "/taller/neumaticos" if params[:proc] == "list"
    end
    session[:cliente_id] = cliente.id if cliente
    redirect_to path
  end

  def agregar

  end

  def pedidos
    @pedidos = Pedido.where(cliente_id: params[:id])
    @cliente = @pedidos.first.cliente rescue Cliente.find(params[:id])
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_cliente
    @cliente = Cliente.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cliente_params
    params.require(:cliente).permit(:email, :tipo_documento, :dni, :nombre_empresa, :razon_fiscal, :calle, :codigo_postal, :localidad, :provincia, :pais, :telefono, :movil, :fax, :condiciones_venta, :descuento, :fecha_alta, :riesgo)
  end
end
