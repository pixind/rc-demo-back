class PedidosController < ApplicationController
  before_action :set_pedido, only: [:show, :edit, :update, :destroy]

  def index
    @pedidos = Pedido.all.limit(150)
  end

  def show
  end

  def confirmar
     if params[:id].to_i == 0
       @pre_pedido = @shopping_cart
     else
       @pre_pedido = Presupuesto.find(params[:id])
     end
  end

  def
  generar
    if params[:type] == "ShoppingCart"
      @pre_pedido = @shopping_cart
    else
      @pre_pedido = Presupuesto.find(params[:id])
    end

    datos = {
        cliente_id: @cliente.id,
        importe: @pre_pedido.total,
        forma_pago: 'ingreso',
        status_id: 1,
        usuario_alta: "web",
        descuento: 0,
        fecha_alta: Time.now,
        importe: @pre_pedido.total_general,
    }

   @pedido = Pedido.new(datos)
   @pedido.agregar_lineas_desde_carro(@pre_pedido)

    if @pedido.save!
      @cliente.presupuestos.destroy_all
      @shopping_cart.clear
      redirect_to "/pedidos/#{@pedido.id}"
    else
      redirect_to "/pedidos/confirmar"
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pedido
      @pedido = Pedido.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pedido_params
      params.require(:pedido).permit(:pid, :cliente_id, :taller_id, :direccion_id, :taller_nuevo_id, :neumatico_id, :status_id, :envio_id, :cupon_id, :test_id, :entrega_id, :forma_pago, :descuento, :descuento_global, :cupon, :importe, :observaciones_entrega, :estado, :factura, :usuario_alta, :fecha_alta, :fecha_pago, :fecha_envio, :fecha_entrega, :fecha_mod, :fecha_factura, :entrada_almacen, :usuario_mod, :fecha_baja, :usuario_baja, :manual, :aplicar_nfu, :iva, :tarifa, :rappel, :pagado, :agrupado, :contactado_telefonico, :contacto_email, :seguimiento, :afiliacion_id, :cancelado, :gastos, :notificar_cancelar, :email_date, :comentarios)
    end
end
