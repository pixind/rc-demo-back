class TalleresController < ApplicationController

  ##
  # Talleres

  # Inicio
  def index
    @pendientes_entrar = Pedido.where(pagado: false)
    @pendientes_montar = Pedido.where(pagado: true)
  end

  def pedido_recibido
    pedido = Pedido.find(params[:id])
    pedido.update_attributes(pagado: true)
    pedido.save!
    redirect_to taller_inicio_path
  end

  def pedido_montado
    pedido = Pedido.find(params[:id])
    pedido.update_attributes(pagado: nil)
    pedido.save!
    redirect_to taller_inicio_path
  end

  def pedido
    @pedido = Pedido.find(params[:id])
  end

  def neumaticos
    @filtros_form = FiltrosForm.new
    @filtros ||= Filtros.new
    @cliente = Cliente.first unless @cliente.id # assure to load the first client if not selected, preventing view crash

    session[:taxes] = false if params[:taller]
    session[:taxes] = true if params[:cliente]

    if request.post?

      @filtros.marca = params[:filtros][:marca]
      @filtros.anchura = params[:filtros][:anchura]
      @filtros.altura = params[:filtros][:altura]
      @filtros.diametro = params[:filtros][:diametro]
      @filtros.carga = params[:filtros][:carga]
      @filtros.temporada = params[:filtros][:temporada]
      @filtros.entrega = params[:filtros][:entrega]
      @filtros.velocidad  = params[:filtros][:velocidad]

      @neumaticos = Tyre.activos.coche.ordenados.filtrados(@filtros).limit(50)

    else
      @neumaticos = []
    end

  end

  def neumatico
     @neumatico = Tyre.find(params[:id])
  end

  def preferencias
    @taller = Cliente.first
  end

  def suplementos
    montaje = params[:montaje] == "on" ? true : false
    aleacion = params[:aleacion] == "on" ? true : false
    nitrogeno = params[:nitrogeno] == "on" ? true : false

    @prefs.update_attributes montaje: montaje, aleacion: aleacion, nitrogeno: nitrogeno
    @prefs.save!

    @shopping_cart.update_attributes montaje: montaje, aleacion: aleacion, nitrogeno: nitrogeno
    @shopping_cart.save!

    redirect_to carrito_path
  end

  def objetivos
    
  end

  # Web
end
