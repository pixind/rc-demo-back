class WebController < ApplicationController

  def selector
    @prefs = Web.first
    if request.post?
      home = params[:home] ||= "default"
      listado = params[:listado] ||= "default"
      detalle = params[:detalle] ||= "default"

      @prefs.update_attributes(home: home, listado: listado, detalle: detalle)
      @prefs.save!
    end
  end
end
