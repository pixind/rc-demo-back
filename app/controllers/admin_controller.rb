class AdminController < ApplicationController

  def inicio

  end

  def neumaticos

  end

  def distribuidores

  end

  def talleres
    @clientes = Cliente.all.limit(30)
  end

  def pedidos

  end

  def preferencias
    @taller = Cliente.first
  end

  def web

  end


  def expedicion_listado
    @pedidos = Pedido.all.limit(15)
  end

  def expedicion_pendientes
    @pedidos = Pedido.all.limit(6)

  end

  def expedicion_enviados
    @pedidos = Pedido.all.limit(11)
  end

end




