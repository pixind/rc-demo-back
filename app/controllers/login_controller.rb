class LoginController < ApplicationController

  def index
     if request.post?

       if ['taller','taller1','taller2','taller3', 'almacen', 'gerencia','financiero','admin','comercial'].include?(params[:user])

         if params[:user] == "taller1"
           site = DtsCmsAdmin::Site.find_by(domain: "taller-ejemplo-1.dev")
           session[:taller_logeado_id] = site.id
         end

         if params[:user] == "taller2"
           site = DtsCmsAdmin::Site.find_by(domain: "taller-ejemplo-2.dev")
           session[:taller_logeado_id] = site.id
         end

         if params[:user] == "taller3"
           site = DtsCmsAdmin::Site.find_by(domain: "taller-ejemplo-3.dev")
           session[:taller_logeado_id] = site.id
         end

         redirect_to taller_neumaticos_path(cliente: true) if ['taller','taller1','taller2','taller3'].include?(params[:user])

         redirect_to almacen_inicio_path if params[:user] == "almacen"
         redirect_to gerencia_inicio_path if params[:user] == "gerencia"
         redirect_to financiero_inicio_path if params[:user] == "financiero"
         redirect_to admin_neumaticos_path if params[:user] == "admin"
         redirect_to comercial_neumaticos_path if params[:user] == "comercial"
       else
         @mensaje = "El usuario no existe"
       end
     else
       #acts_as_logout
       session[:taller_logeado_id] = nil
     end
  end

end

