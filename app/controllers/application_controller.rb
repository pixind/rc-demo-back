class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception

  before_filter :extract_shopping_cart, :set_client, :set_preferences

  private
  def extract_shopping_cart
    shopping_cart_id = session[:shopping_cart_id]
    @shopping_cart = session[:shopping_cart_id] ? ShoppingCart.find(shopping_cart_id) : ShoppingCart.create
    session[:shopping_cart_id] = @shopping_cart.id
  end

  def set_client
    cliente_id = session[:cliente_id]
    @cliente = session[:cliente_id] ? Cliente.find(cliente_id) : Cliente.new
  end

  def set_preferences
    @prefs ||= Prefs.first
  end

end
