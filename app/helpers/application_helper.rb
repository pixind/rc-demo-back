module ApplicationHelper

  def title(value)
    unless value.nil?
      @title = "#{value} | Api"
    end
  end

  def boolean(object)
    state = "<i class='fa fa-circle' style=color:grey;></i>".html_safe if object == true
    state = "<i class='fa fa-circle-o' style=color:grey;></i>".html_safe if object == false
    state
  end

  def os(object)
    state = "<i class='fa fa-android' style=color:grey;></i>".html_safe if object == 'android'
    state = "<i class='fa fa-apple' style=color:grey;></i>".html_safe if object == 'ios'
    state
  end

  def date(date)
    date.strftime("%d-%m-%Y") rescue nil
  end

  def datetime(date)
    date.strftime("%d/%m/%Y %I:%M%p") rescue nil
  end

  def currency(value)
    number_to_currency(value, unit:'') + "€"
  end

  def clima(temporada,style="")
    return "<i class='fa fa-cloud #{style}' style='color:#79b9e2;'></i> Invierno" if temporada == "Invierno"
    return "<i class='fa fa-sun-o #{style}' style='color:#f5cb24;'></i> Verano" if temporada == "Verano"
  end

  def precio_neumatico(neumatico,taxes)
    if taxes
      number_to_currency(neumatico.pvp_t1_iva + (neumatico.pvp_t1_iva * 0.21), unit: "")
    else
      number_to_currency(neumatico.pvp_t1_iva, unit: "")
    end
    #number_to_currency(neumatico.pvp_t1_iva + (neumatico.pvp_t1_iva * 0.21), unit: "") if taxes == true
    #number_to_currency(neumatico.pvp_t1_iva, unit: "") if taxes == false
  end

  def caracteristicas
    %{<i class='fa fa-volume-down fa-2x' style='color:#{ [true, false].sample ? "#c9c9c9" : "#43975f" };'></i> <i class='fa fa-umbrella fa-2x' style='color:#{ [true, false].sample ? "#c9c9c9" : "#43975f" };'></i> <i class='fa fa-shield fa-2x' style='color:#{ [true, false].sample ? "#c9c9c9" : "#43975f" };'></i>}
  end


end
