class Filtros

  attr_accessor :marca, :anchura, :altura, :diametro, :carga, :runflat, :temporada, :entrega, :velocidad

  def initialize
    @marca = "%"
    @anchura = 195
    @altura = 65
    @diametro = 15
    @carga = 52
    @temporada = nil
    @entrega = nil
    @velocidad  = nil
  end

  MARCA = Tyre.select("DISTINCT marca").order('marca ASC').map do |f| {:value => f.marca, :description => f.marca.capitalize} end
  ANCHURA = Tyre.select("DISTINCT anchura").where("tipo<>'moto'").order('anchura+0 ASC').map {|f| f.anchura}
  ALTURA = Tyre.select("DISTINCT altura").where("tipo<>'moto'").order('altura+0 ASC').map {|f| f.altura}
  DIAMETRO = Tyre.select("DISTINCT diametro").where("tipo<>'moto'").order('diametro+0 ASC').map {|f| f.diametro}
  CARGA =Tyre.select("DISTINCT carga").where("tipo<>'moto'").order('carga+0 ASC').map do |f| {:value => f.carga, :description => f.carga} end

  RUNFLAT = [{:value => 0, :description => "NO"},
             {:value => 1, :description => "SI"}]

  TEMPORADA = [{:value => "Invierno", :description => "Invierno"},
               {:value => "Verano", :description => "Verano"},
               {:value => "Todo tiemp", :description => "Todo Tiempo"}]

  VELOCIDAD = [{:value => "%%%",  :description => "Todas"},
               {:value => "Y",  :description => "Y - 300 km/h" },
               {:value => "W",  :description => "W - 270 km/h" },
               {:value => "ZR", :description => "ZR - 240 km/h"},
               {:value => "Z",  :description => "Z - 240 km/h" },
               {:value => "V",  :description => "V - 240 km/h" },
               {:value => "H",  :description => "H - 210 km/h" },
               {:value => "T",  :description => "T - 190 km/h" },
               {:value => "S",  :description => "S - 180 km/h" },
               {:value => "R",  :description => "R - 170 km/h" },
               {:value => "Q",  :description => "Q - 160 km/h" },
               {:value => "P",  :description => "P - 150 km/h" },
               {:value => "N",  :description => "N - 140 km/h" },
               {:value => "M",  :description => "M - 130 km/h" },
               {:value => "L",  :description => "L - 120 km/h" },
               {:value => "K",  :description => "K - 110 km/h" },
               {:value => "J",  :description => "J - 100 km/h" }]

  VELOCIDAD_MOTO = [{:value => "%%%",  :description => "Todas"},
                    {:value => "W",  :description => "W - 270 km/h" },
                    {:value => "ZR", :description => "ZR - 240 km/h"},
                    {:value => "Z",  :description => "Z - 240 km/h" },
                    {:value => "V",  :description => "V - 240 km/h" },
                    {:value => "H",  :description => "H - 210 km/h" },
                    {:value => "T",  :description => "T - 190 km/h" },
                    {:value => "S",  :description => "S - 180 km/h" },
                    {:value => "R",  :description => "R - 170 km/h" },
                    {:value => "Q",  :description => "Q - 160 km/h" },
                    {:value => "P",  :description => "P - 150 km/h" },
                    {:value => "N",  :description => "N - 140 km/h" },
                    {:value => "M",  :description => "M - 130 km/h" },
                    {:value => "L",  :description => "L - 120 km/h" },
                    {:value => "J",  :description => "J - 100 km/h" },
                    {:value => "E",  :description => "E - 70 km/h" },
                    {:value => "F",  :description => "F - 80 km/h" },
                    {:value => "B",  :description => "B - 50 km/h" },
                    {:value => "",  :description => "" }]

  CAMBIO_NEUMATICO = [["menos de 1 año"],["entre 1 y 2 años"],["más de 2 años"],["nunca"]]
  KM_ANUALES = [["menos de 10.000"],["entre 10.000 y 20.000"],["entre 20.000 y 30.000"],["más de 30.000"]]

  def self.velocidad(velocidad,tipo)
    if tipo == "moto"
      velocidades = case velocidad
                      when ""    then "('')"
                      when "W"   then "('W')"
                      when "ZR"  then "('ZR','W')"
                      when "Z"   then "('Z','ZR','W')"
                      when "V"   then "('V','Z','ZR','W')"
                      when "H"   then "('H','V','Z','ZR','W')"
                      when "T"   then "('T','H','V','Z','ZR','W')"
                      when "S"   then "('S','T','H','V','Z','ZR','W')"
                      when "R"   then "('R','S','T','H','V','Z','ZR','W')"
                      when "Q"   then "('Q','R','S','T','H','V','Z','ZR','W')"
                      when "P"   then "('P','Q','R','S','T','H','V','Z','ZR','W')"
                      when "N"   then "('N','P','Q','R','S','T','H','V','Z','ZR','W')"
                      when "M"   then "('M','N','P','Q','R','S','T','H','V','Z','ZR','W')"
                      when "L"   then "('L','M','N','P','Q','R','S','T','H','V','Z','ZR','W')"
                      when "J"   then "('J','L','M','N','P','Q','R','S','T','H','V','Z','ZR','W')"
                      when "E"   then "('E','J','L','M','N','P','Q','R','S','T','H','V','Z','ZR','W')"
                      when "F"   then "('F','E','J','L','M','N','P','Q','R','S','T','H','V','Z','ZR','W')"
                      when "B"   then "('B','F','E','J','L','M','N','P','Q','R','S','T','H','V','Z','ZR','W')"
                      when "%%%" then "('B','F','E','J','L','M','N','P','Q','R','S','T','H','V','Z','ZR','W','')"
                    end
      velocidades ||= "('B','F','E','J','L','M','N','P','Q','R','S','T','H','V','Z','ZR','W','')"
    else
      velocidades = case velocidad
                      when ""    then  "('')"
                      when "Y"   then  "('Y')"
                      when "W"   then  "('W','Y')"
                      when "ZR"  then  "('Z','ZR','W','Y')"
                      when "Z"   then  "('Z','ZR','W','Y')"
                      when "V"   then  "('V','Z','ZR','W','Y')"
                      when "H"   then  "('H','V','Z','ZR','W','Y')"
                      when "T"   then  "('T','H','V','Z','ZR','W','Y')"
                      when "S"   then  "('S','T','H','V','Z','ZR','W','Y')"
                      when "R"   then  "('R','S','T','H','V','Z','ZR','W','Y')"
                      when "Q"   then  "('Q','R','S','T','H','V','Z','ZR','W','Y')"
                      when "P"   then  "('P','Q','R','S','T','H','V','Z','ZR','W','Y')"
                      when "N"   then  "('N','P','Q','R','S','T','H','V','Z','ZR','W','Y')"
                      when "M"   then  "('M','N','P','Q','R','S','T','H','V','Z','ZR','W','Y')"
                      when "L"   then  "('L','M','N','P','Q','R','S','T','H','V','Z','ZR','W','Y')"
                      when "K"   then  "('K','L','M','N','P','Q','R','S','T','H','V','Z','ZR','W','Y')"
                      when "J"   then  "('J','K','L','M','N','P','Q','R','S','T','H','V','Z','ZR','W','Y')"
                      when "%%%" then  "('J','K','L','M','N','P','Q','R','S','T','H','V','Z','R','W','Y','')"
                    end
      velocidades ||= "('J','K','L','M','N','P','Q','R','S','T','H','V','Z','R','W','Y')"
    end
  end

  def self.tipo(tipo)
    return "tipo='Moto'" if tipo == "moto"
    return "tipo<>'Moto'" if tipo != "moto"
    return "" if tipo == "mixed"
  end

  def self.check(data)
    data = "%%%" if (data == "" || data.nil?)
    return data
  end

end
