class Pedido < ActiveRecord::Base
  belongs_to :cliente
  has_many :pedido_detalles

  def agregar_lineas_desde_carro(cart)
    cart.items.each do |item|
      neumatico = Tyre.find(item.item_id)
      li = PedidoDetalle.desde_el_carro(item,neumatico)
      pedido_detalles << li
    end
  end

  # =======================
  # = Calculos de pedidos =
  # =======================

  def activo(id)
    return Pedido.find(id)
  end

  def total_articulos
    pedido_detalles.each.sum { |item| item.total_articulo }
  end

  def total_items
    pedido_detalles.each.sum { |item| item.unidades }
  end

  def nfu
    nfu = {}
    nfu[:A] = {:cantidad => pedido_detalle.find_all { |i| i.tipo == "Moto" }.sum { |item| item.unidades },
               :valor => pedido_detalle.find_all { |i| i.tipo == "Moto" }.sum { |item| item.nfu * item.unidades }}
    nfu[:B] = {:cantidad => pedido_detalle.find_all { |i| i.tipo == "Turismo" }.sum { |item| item.unidades },
               :valor => pedido_detalle.find_all { |i| i.tipo == "Turismo" }.sum { |item| item.nfu * item.unidades }}
    nfu[:C] = {:cantidad => pedido_detalle.find_all { |i| (i.tipo == "Furgoneta" || i.tipo == "4x4") }.sum { |item| item.unidades }, :valor => pedido_detalle.find_all { |i| (i.tipo == "Furgoneta" || i.tipo == "4x4") }.sum { |item| item.nfu * item.unidades }}
    nfu[:D] = {:cantidad => pedido_detalle.find_all { |i| i.tipo == "Camion" }.sum { |item| item.unidades },
               :valor => pedido_detalle.find_all { |i| i.tipo == "Camion" }.sum { |item| item.nfu * item.unidades }}

    return nfu
  end

  def total_nfu
    pedido_detalles.each.sum { |item| item.nfu * item.unidades }
  end

  def base_imponible
    total_articulos + total_nfu + gastos
  end

  def importe_iva
    calc_iva = case iva
                 when 16 then
                   0.16
                 when 18 then
                   0.18
                 when 21 then
                   0.21
               end
    base_imponible * calc_iva
  end

  def total
    base_imponible + importe_iva
  end


end
