class Presupuesto < ActiveRecord::Base
  belongs_to :cliente
  has_many :presupuesto_items

  def items
     self.presupuesto_items
  end

  def taxes
    0
  end

  def total_nfu
    self.total_unique_items * 1.58
  end

  def base_imponible
    total_nfu + self.total
  end

  def iva
    base_imponible * 0.21
  end

  def total_general
    total_nfu + base_imponible + iva
  end

  def total_unique_items
    presupuesto_items.collect(&:quantity).inject(:+) rescue 0
  end

  def total
    self.presupuesto_items.collect { |i| i.quantity * i.price }.inject(:+) rescue 0
  end


end
