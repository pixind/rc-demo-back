class ShoppingCart < ActiveRecord::Base

  acts_as_shopping_cart

  def items
     self.shopping_cart_items
  end

  def taxes
    0
  end


  def total_montaje
    self.montaje ? total_unique_items * 15 : 0
  end

  def total_aleacion
    self.aleacion ? total_unique_items * 1 : 0
  end

  def total_nitrogeno
    self.nitrogeno ? total_unique_items * 1.5 : 0
  end


  def total_nfu
    self.total_unique_items * 1.58
  end

  def base_imponible
    total_nfu + self.total + total_montaje + total_aleacion + total_aleacion
  end

  def iva
    base_imponible * 0.21
  end

  def total_general
    total_nfu + base_imponible + iva
  end

end
