class PedidoDetalle < ActiveRecord::Base
  belongs_to :pedido

  def self.desde_el_carro(cart_item,neumatico)
    li = self.new
    li.unidades         = cart_item.quantity
    li.marca            = neumatico.marca
    li.caracteristica_1 = neumatico.caracteristica_1
    li.caracteristica_2 = neumatico.caracteristica_2
    li.caracteristica_3 = neumatico.caracteristica_3
    li.articulo_id      = neumatico.id
    li.tipo             = neumatico.tipo
    li.modelo           = neumatico.modelo
    li.anchura          = neumatico.anchura
    li.altura           = neumatico.altura
    li.diametro         = neumatico.diametro
    li.velocidad        = neumatico.velocidad
    li.carga            = neumatico.carga
    li.temporada        = neumatico.temporada
    li.precio           = cart_item.price
    li.origen           = neumatico.origen
    li.precio_origen    = neumatico.precio_origen
    li.descuento        = neumatico.descuento
    li.total_articulo   = cart_item.price * cart_item.quantity
    li.nfu              = "0"
    li
  end

end
