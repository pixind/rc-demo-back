class Tyre < ActiveRecord::Base

  scope :red, -> { where(color: 'red') }
  scope :dry_clean_only, -> { joins(:washing_instructions).where('washing_instructions.dry_clean_only = ?', true) }


  scope :activos, -> { where(activo: true) }
  scope :coche, -> { where(['tipo <> ?', "moto"]) }
  scope :ordenados, -> { order("entrega ASC, oferta DESC, recomendado DESC, pvp_iva ASC") }

  #scope :filtrados, ->(filtros) {[anchura: filtros.anchura ]}

  def self.filtrados(filtros)
       where("marca LIKE ? AND
            anchura LIKE ? AND
             altura LIKE ? AND
           diametro LIKE ? AND
              carga >= ? AND
          temporada LIKE ? AND
            entrega LIKE ? AND
          velocidad IN #{Filtros.velocidad(filtros.velocidad,"coche")}",
             Filtros.check(filtros.marca),
                     filtros.anchura,
                     filtros.altura,
                     filtros.diametro,
                     Filtros.check(filtros.carga),
                     Filtros.check(filtros.temporada),
                     Filtros.check(filtros.entrega)

       )
  end

end