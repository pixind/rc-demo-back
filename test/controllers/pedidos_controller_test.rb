require 'test_helper'

class PedidosControllerTest < ActionController::TestCase
  setup do
    @pedido = pedidos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pedidos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pedido" do
    assert_difference('Pedido.count') do
      post :create, pedido: { afiliacion_id: @pedido.afiliacion_id, agrupado: @pedido.agrupado, aplicar_nfu: @pedido.aplicar_nfu, cancelado: @pedido.cancelado, cliente_id: @pedido.cliente_id, comentarios: @pedido.comentarios, contactado_telefonico: @pedido.contactado_telefonico, contacto_email: @pedido.contacto_email, cupon: @pedido.cupon, cupon_id: @pedido.cupon_id, descuento: @pedido.descuento, descuento_global: @pedido.descuento_global, direccion_id: @pedido.direccion_id, email_date: @pedido.email_date, entrada_almacen: @pedido.entrada_almacen, entrega_id: @pedido.entrega_id, envio_id: @pedido.envio_id, estado: @pedido.estado, factura: @pedido.factura, fecha_alta: @pedido.fecha_alta, fecha_baja: @pedido.fecha_baja, fecha_entrega: @pedido.fecha_entrega, fecha_envio: @pedido.fecha_envio, fecha_factura: @pedido.fecha_factura, fecha_mod: @pedido.fecha_mod, fecha_pago: @pedido.fecha_pago, forma_pago: @pedido.forma_pago, gastos: @pedido.gastos, importe: @pedido.importe, iva: @pedido.iva, manual: @pedido.manual, neumatico_id: @pedido.neumatico_id, notificar_cancelar: @pedido.notificar_cancelar, observaciones_entrega: @pedido.observaciones_entrega, pagado: @pedido.pagado, pid: @pedido.pid, rappel: @pedido.rappel, seguimiento: @pedido.seguimiento, status_id: @pedido.status_id, taller_id: @pedido.taller_id, taller_nuevo_id: @pedido.taller_nuevo_id, tarifa: @pedido.tarifa, test_id: @pedido.test_id, usuario_alta: @pedido.usuario_alta, usuario_baja: @pedido.usuario_baja, usuario_mod: @pedido.usuario_mod }
    end

    assert_redirected_to pedido_path(assigns(:pedido))
  end

  test "should show pedido" do
    get :show, id: @pedido
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pedido
    assert_response :success
  end

  test "should update pedido" do
    patch :update, id: @pedido, pedido: { afiliacion_id: @pedido.afiliacion_id, agrupado: @pedido.agrupado, aplicar_nfu: @pedido.aplicar_nfu, cancelado: @pedido.cancelado, cliente_id: @pedido.cliente_id, comentarios: @pedido.comentarios, contactado_telefonico: @pedido.contactado_telefonico, contacto_email: @pedido.contacto_email, cupon: @pedido.cupon, cupon_id: @pedido.cupon_id, descuento: @pedido.descuento, descuento_global: @pedido.descuento_global, direccion_id: @pedido.direccion_id, email_date: @pedido.email_date, entrada_almacen: @pedido.entrada_almacen, entrega_id: @pedido.entrega_id, envio_id: @pedido.envio_id, estado: @pedido.estado, factura: @pedido.factura, fecha_alta: @pedido.fecha_alta, fecha_baja: @pedido.fecha_baja, fecha_entrega: @pedido.fecha_entrega, fecha_envio: @pedido.fecha_envio, fecha_factura: @pedido.fecha_factura, fecha_mod: @pedido.fecha_mod, fecha_pago: @pedido.fecha_pago, forma_pago: @pedido.forma_pago, gastos: @pedido.gastos, importe: @pedido.importe, iva: @pedido.iva, manual: @pedido.manual, neumatico_id: @pedido.neumatico_id, notificar_cancelar: @pedido.notificar_cancelar, observaciones_entrega: @pedido.observaciones_entrega, pagado: @pedido.pagado, pid: @pedido.pid, rappel: @pedido.rappel, seguimiento: @pedido.seguimiento, status_id: @pedido.status_id, taller_id: @pedido.taller_id, taller_nuevo_id: @pedido.taller_nuevo_id, tarifa: @pedido.tarifa, test_id: @pedido.test_id, usuario_alta: @pedido.usuario_alta, usuario_baja: @pedido.usuario_baja, usuario_mod: @pedido.usuario_mod }
    assert_redirected_to pedido_path(assigns(:pedido))
  end

  test "should destroy pedido" do
    assert_difference('Pedido.count', -1) do
      delete :destroy, id: @pedido
    end

    assert_redirected_to pedidos_path
  end
end
