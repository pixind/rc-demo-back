require 'test_helper'

class ClientesControllerTest < ActionController::TestCase
  setup do
    @cliente = clientes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:clientes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cliente" do
    assert_difference('Cliente.count') do
      post :create, cliente: { calle: @cliente.calle, codigo_postal: @cliente.codigo_postal, condiciones_venta: @cliente.condiciones_venta, descuento: @cliente.descuento, dni: @cliente.dni, email: @cliente.email, fax: @cliente.fax, fecha_alta: @cliente.fecha_alta, localidad: @cliente.localidad, movil: @cliente.movil, nombre_empresa: @cliente.nombre_empresa, pais: @cliente.pais, provincia: @cliente.provincia, razon_fiscal: @cliente.razon_fiscal, riesgo: @cliente.riesgo, telefono: @cliente.telefono, tipo_documento: @cliente.tipo_documento }
    end

    assert_redirected_to cliente_path(assigns(:cliente))
  end

  test "should show cliente" do
    get :show, id: @cliente
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cliente
    assert_response :success
  end

  test "should update cliente" do
    patch :update, id: @cliente, cliente: { calle: @cliente.calle, codigo_postal: @cliente.codigo_postal, condiciones_venta: @cliente.condiciones_venta, descuento: @cliente.descuento, dni: @cliente.dni, email: @cliente.email, fax: @cliente.fax, fecha_alta: @cliente.fecha_alta, localidad: @cliente.localidad, movil: @cliente.movil, nombre_empresa: @cliente.nombre_empresa, pais: @cliente.pais, provincia: @cliente.provincia, razon_fiscal: @cliente.razon_fiscal, riesgo: @cliente.riesgo, telefono: @cliente.telefono, tipo_documento: @cliente.tipo_documento }
    assert_redirected_to cliente_path(assigns(:cliente))
  end

  test "should destroy cliente" do
    assert_difference('Cliente.count', -1) do
      delete :destroy, id: @cliente
    end

    assert_redirected_to clientes_path
  end
end
