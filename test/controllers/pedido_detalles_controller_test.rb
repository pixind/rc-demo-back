require 'test_helper'

class PedidoDetallesControllerTest < ActionController::TestCase
  setup do
    @pedido_detalle = pedido_detalles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pedido_detalles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pedido_detalle" do
    assert_difference('PedidoDetalle.count') do
      post :create, pedido_detalle: { albaran: @pedido_detalle.albaran, almacen_id: @pedido_detalle.almacen_id, altura: @pedido_detalle.altura, anchura: @pedido_detalle.anchura, articulo_id: @pedido_detalle.articulo_id, asignado: @pedido_detalle.asignado, caracteristica_1: @pedido_detalle.caracteristica_1, caracteristica_2: @pedido_detalle.caracteristica_2, caracteristica_3: @pedido_detalle.caracteristica_3, carga: @pedido_detalle.carga, cod_gm: @pedido_detalle.cod_gm, coste_transporte: @pedido_detalle.coste_transporte, descuento: @pedido_detalle.descuento, diametro: @pedido_detalle.diametro, entrada_almacen: @pedido_detalle.entrada_almacen, marca: @pedido_detalle.marca, modelo: @pedido_detalle.modelo, nfu: @pedido_detalle.nfu, origen: @pedido_detalle.origen, pedido_id: @pedido_detalle.pedido_id, pid: @pedido_detalle.pid, precio: @pedido_detalle.precio, precio_descuento: @pedido_detalle.precio_descuento, precio_origen: @pedido_detalle.precio_origen, proveedor_id: @pedido_detalle.proveedor_id, pulgadas: @pedido_detalle.pulgadas, temporada: @pedido_detalle.temporada, tipo: @pedido_detalle.tipo, tipo_llanta: @pedido_detalle.tipo_llanta, total_articulo: @pedido_detalle.total_articulo, track: @pedido_detalle.track, transporte: @pedido_detalle.transporte, transporte_id: @pedido_detalle.transporte_id, unidades: @pedido_detalle.unidades, velocidad: @pedido_detalle.velocidad }
    end

    assert_redirected_to pedido_detalle_path(assigns(:pedido_detalle))
  end

  test "should show pedido_detalle" do
    get :show, id: @pedido_detalle
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pedido_detalle
    assert_response :success
  end

  test "should update pedido_detalle" do
    patch :update, id: @pedido_detalle, pedido_detalle: { albaran: @pedido_detalle.albaran, almacen_id: @pedido_detalle.almacen_id, altura: @pedido_detalle.altura, anchura: @pedido_detalle.anchura, articulo_id: @pedido_detalle.articulo_id, asignado: @pedido_detalle.asignado, caracteristica_1: @pedido_detalle.caracteristica_1, caracteristica_2: @pedido_detalle.caracteristica_2, caracteristica_3: @pedido_detalle.caracteristica_3, carga: @pedido_detalle.carga, cod_gm: @pedido_detalle.cod_gm, coste_transporte: @pedido_detalle.coste_transporte, descuento: @pedido_detalle.descuento, diametro: @pedido_detalle.diametro, entrada_almacen: @pedido_detalle.entrada_almacen, marca: @pedido_detalle.marca, modelo: @pedido_detalle.modelo, nfu: @pedido_detalle.nfu, origen: @pedido_detalle.origen, pedido_id: @pedido_detalle.pedido_id, pid: @pedido_detalle.pid, precio: @pedido_detalle.precio, precio_descuento: @pedido_detalle.precio_descuento, precio_origen: @pedido_detalle.precio_origen, proveedor_id: @pedido_detalle.proveedor_id, pulgadas: @pedido_detalle.pulgadas, temporada: @pedido_detalle.temporada, tipo: @pedido_detalle.tipo, tipo_llanta: @pedido_detalle.tipo_llanta, total_articulo: @pedido_detalle.total_articulo, track: @pedido_detalle.track, transporte: @pedido_detalle.transporte, transporte_id: @pedido_detalle.transporte_id, unidades: @pedido_detalle.unidades, velocidad: @pedido_detalle.velocidad }
    assert_redirected_to pedido_detalle_path(assigns(:pedido_detalle))
  end

  test "should destroy pedido_detalle" do
    assert_difference('PedidoDetalle.count', -1) do
      delete :destroy, id: @pedido_detalle
    end

    assert_redirected_to pedido_detalles_path
  end
end
