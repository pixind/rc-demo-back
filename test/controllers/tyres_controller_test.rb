require 'test_helper'

class TyresControllerTest < ActionController::TestCase
  setup do
    @tyre = tyres(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tyres)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tyre" do
    assert_difference('Tyre.count') do
      post :create, tyre: { activo: @tyre.activo, altura: @tyre.altura, anchura: @tyre.anchura, caracteristica_1: @tyre.caracteristica_1, caracteristica_2: @tyre.caracteristica_2, caracteristica_3: @tyre.caracteristica_3, carga: @tyre.carga, categoria_peso: @tyre.categoria_peso, codigo_fabricante: @tyre.codigo_fabricante, descuento: @tyre.descuento, diametro: @tyre.diametro, ean: @tyre.ean, entrega: @tyre.entrega, foto: @tyre.foto, id: @tyre.id, logo_marca: @tyre.logo_marca, marca: @tyre.marca, modelo: @tyre.modelo, mojado: @tyre.mojado, nfu: @tyre.nfu, oferta: @tyre.oferta, origen: @tyre.origen, precio_origen: @tyre.precio_origen, pvp: @tyre.pvp, pvp_iva: @tyre.pvp_iva, pvp_t1: @tyre.pvp_t1, pvp_t1_iva: @tyre.pvp_t1_iva, pvp_t2: @tyre.pvp_t2, pvp_t2_iva: @tyre.pvp_t2_iva, pvp_t3: @tyre.pvp_t3, pvp_t3_iva: @tyre.pvp_t3_iva, recomendado: @tyre.recomendado, resistencia: @tyre.resistencia, ruido: @tyre.ruido, runflat: @tyre.runflat, stock: @tyre.stock, temporada: @tyre.temporada, tid: @tyre.tid, tipo: @tyre.tipo, unidades_defecto: @tyre.unidades_defecto, velocidad: @tyre.velocidad }
    end

    assert_redirected_to tyre_path(assigns(:tyre))
  end

  test "should show tyre" do
    get :show, id: @tyre
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tyre
    assert_response :success
  end

  test "should update tyre" do
    patch :update, id: @tyre, tyre: { activo: @tyre.activo, altura: @tyre.altura, anchura: @tyre.anchura, caracteristica_1: @tyre.caracteristica_1, caracteristica_2: @tyre.caracteristica_2, caracteristica_3: @tyre.caracteristica_3, carga: @tyre.carga, categoria_peso: @tyre.categoria_peso, codigo_fabricante: @tyre.codigo_fabricante, descuento: @tyre.descuento, diametro: @tyre.diametro, ean: @tyre.ean, entrega: @tyre.entrega, foto: @tyre.foto, id: @tyre.id, logo_marca: @tyre.logo_marca, marca: @tyre.marca, modelo: @tyre.modelo, mojado: @tyre.mojado, nfu: @tyre.nfu, oferta: @tyre.oferta, origen: @tyre.origen, precio_origen: @tyre.precio_origen, pvp: @tyre.pvp, pvp_iva: @tyre.pvp_iva, pvp_t1: @tyre.pvp_t1, pvp_t1_iva: @tyre.pvp_t1_iva, pvp_t2: @tyre.pvp_t2, pvp_t2_iva: @tyre.pvp_t2_iva, pvp_t3: @tyre.pvp_t3, pvp_t3_iva: @tyre.pvp_t3_iva, recomendado: @tyre.recomendado, resistencia: @tyre.resistencia, ruido: @tyre.ruido, runflat: @tyre.runflat, stock: @tyre.stock, temporada: @tyre.temporada, tid: @tyre.tid, tipo: @tyre.tipo, unidades_defecto: @tyre.unidades_defecto, velocidad: @tyre.velocidad }
    assert_redirected_to tyre_path(assigns(:tyre))
  end

  test "should destroy tyre" do
    assert_difference('Tyre.count', -1) do
      delete :destroy, id: @tyre
    end

    assert_redirected_to tyres_path
  end
end
