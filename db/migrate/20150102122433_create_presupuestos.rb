class CreatePresupuestos < ActiveRecord::Migration
  def change
    create_table :presupuestos do |t|
      t.integer :cliente_id

      t.timestamps
    end
  end
end
