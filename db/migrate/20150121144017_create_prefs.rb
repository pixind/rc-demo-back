class CreatePrefs < ActiveRecord::Migration
  def change
    create_table :prefs do |t|
      t.boolean :montaje
      t.boolean :aleacion
      t.boolean :nitrogeno
      t.float :precio_hora

      t.timestamps
    end
  end
end
