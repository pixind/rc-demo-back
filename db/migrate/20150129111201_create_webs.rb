class CreateWebs < ActiveRecord::Migration
  def change
    create_table :webs do |t|
      t.string :home
      t.string :listado
      t.string :detalle
      t.string :color_primario
      t.string :color_secundario
      t.string :logo

      t.timestamps
    end
  end
end
