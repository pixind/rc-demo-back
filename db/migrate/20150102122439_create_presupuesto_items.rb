class CreatePresupuestoItems < ActiveRecord::Migration
  def change
    create_table :presupuesto_items do |t|
      t.integer :presupuesto_id
      t.integer :quantity
      t.integer :item_id
      t.float :price

      t.timestamps
    end
  end
end
