class CreateTyres < ActiveRecord::Migration
  def change
    create_table :tyres do |t|
      t.string :tid
      t.string :marca
      t.string :modelo
      t.string :caracteristica_1
      t.string :caracteristica_2
      t.string :caracteristica_3
      t.string :anchura
      t.string :altura
      t.string :diametro
      t.integer :carga
      t.string :velocidad
      t.string :tipo
      t.string :temporada
      t.string :origen
      t.string :runflat
      t.integer :entrega
      t.decimal :precio_origen
      t.integer :stock
      t.integer :pvp
      t.integer :pvp_iva
      t.decimal :pvp_t1
      t.decimal :pvp_t1_iva
      t.decimal :pvp_t2
      t.decimal :pvp_t2_iva
      t.decimal :pvp_t3
      t.decimal :pvp_t3_iva
      t.integer :descuento
      t.integer :unidades_defecto
      t.string :foto
      t.string :logo_marca
      t.string :nfu
      t.integer :activo
      t.string :oferta
      t.boolean :recomendado
      t.string :ean
      t.integer :categoria_peso
      t.string :codigo_fabricante
      t.string :resistencia
      t.string :mojado
      t.string :ruido

      t.timestamps
    end
  end
end
