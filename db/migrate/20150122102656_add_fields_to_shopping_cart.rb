class AddFieldsToShoppingCart < ActiveRecord::Migration
  def change
    add_column :shopping_carts, :montaje, :boolean
    add_column :shopping_carts, :aleacion, :boolean
    add_column :shopping_carts, :nitrogeno, :boolean
  end
end
