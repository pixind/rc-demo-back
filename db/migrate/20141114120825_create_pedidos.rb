class CreatePedidos < ActiveRecord::Migration
  def change
    create_table :pedidos do |t|
      t.string :pid
      t.integer :cliente_id
      t.integer :taller_id
      t.integer :direccion_id
      t.integer :taller_nuevo_id
      t.integer :neumatico_id
      t.integer :status_id
      t.integer :envio_id
      t.integer :cupon_id
      t.integer :test_id
      t.integer :entrega_id
      t.string :forma_pago
      t.integer :descuento
      t.integer :descuento_global
      t.string :cupon
      t.float :importe
      t.text :observaciones_entrega
      t.string :estado
      t.integer :factura
      t.string :usuario_alta
      t.datetime :fecha_alta
      t.datetime :fecha_pago
      t.datetime :fecha_envio
      t.datetime :fecha_entrega
      t.string :fecha_mod
      t.datetime :fecha_factura
      t.datetime :entrada_almacen
      t.string :usuario_mod
      t.datetime :fecha_baja
      t.string :usuario_baja
      t.boolean :manual
      t.boolean :aplicar_nfu
      t.integer :iva
      t.integer :tarifa
      t.integer :rappel
      t.boolean :pagado
      t.boolean :agrupado
      t.boolean :contactado_telefonico
      t.boolean :contacto_email
      t.boolean :seguimiento
      t.integer :afiliacion_id
      t.boolean :cancelado
      t.float :gastos
      t.boolean :notificar_cancelar
      t.datetime :email_date
      t.text :comentarios

      t.timestamps
    end
  end
end
