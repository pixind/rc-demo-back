class CreateClientes < ActiveRecord::Migration
  def change
    create_table :clientes do |t|
      t.string :email
      t.string :tipo_documento
      t.string :dni
      t.string :nombre_empresa
      t.string :razon_fiscal
      t.string :calle
      t.string :codigo_postal
      t.string :localidad
      t.string :provincia
      t.string :pais
      t.string :telefono
      t.string :movil
      t.string :fax
      t.string :condiciones_venta
      t.integer :descuento
      t.datetime :fecha_alta
      t.float :riesgo

      t.timestamps
    end
  end
end
