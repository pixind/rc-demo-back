class CreatePedidoDetalles < ActiveRecord::Migration
  def change
    create_table :pedido_detalles do |t|
      t.string :pid
      t.integer :pedido_id
      t.integer :articulo_id
      t.string :tipo
      t.string :marca
      t.string :modelo
      t.string :caracteristica_1
      t.string :caracteristica_2
      t.string :caracteristica_3
      t.string :anchura
      t.string :altura
      t.string :diametro
      t.string :carga
      t.string :velocidad
      t.string :temporada
      t.string :pulgadas
      t.string :tipo_llanta
      t.integer :unidades
      t.float :precio
      t.float :precio_origen
      t.integer :descuento
      t.string :cod_gm
      t.integer :origen
      t.float :precio_descuento
      t.float :total_articulo
      t.float :nfu
      t.string :transporte
      t.boolean :asignado
      t.integer :proveedor_id
      t.integer :transporte_id
      t.string :track
      t.datetime :entrada_almacen
      t.float :coste_transporte
      t.string :albaran
      t.integer :almacen_id

      t.timestamps
    end
  end
end
