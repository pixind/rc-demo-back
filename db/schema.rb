# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150129111201) do

  create_table "clientes", force: true do |t|
    t.string   "email"
    t.string   "tipo_documento",    limit: 25
    t.string   "dni"
    t.string   "nombre_empresa"
    t.string   "razon_fiscal"
    t.string   "calle"
    t.string   "codigo_postal"
    t.string   "localidad"
    t.string   "provincia"
    t.string   "pais"
    t.string   "telefono"
    t.string   "movil"
    t.string   "fax"
    t.string   "condiciones_venta"
    t.integer  "descuento",                    default: 0,   null: false
    t.datetime "fecha_alta"
    t.float    "riesgo",            limit: 24, default: 0.0
  end

  create_table "pedido_detalles", force: true do |t|
    t.string   "pid",                          default: "ES"
    t.integer  "pedido_id"
    t.integer  "articulo_id"
    t.string   "tipo"
    t.string   "marca"
    t.string   "modelo"
    t.string   "caracteristica_1", limit: 100
    t.string   "caracteristica_2", limit: 100
    t.string   "caracteristica_3", limit: 100
    t.string   "anchura"
    t.string   "altura"
    t.string   "diametro"
    t.string   "carga"
    t.string   "velocidad"
    t.string   "temporada"
    t.string   "pulgadas"
    t.string   "tipo_llanta"
    t.integer  "unidades"
    t.float    "precio",           limit: 24
    t.float    "precio_origen",    limit: 24
    t.integer  "descuento"
    t.string   "cod_gm",           limit: 100
    t.integer  "origen"
    t.float    "precio_descuento", limit: 24
    t.float    "total_articulo",   limit: 24
    t.float    "nfu",              limit: 24
    t.string   "transporte",       limit: 100
    t.boolean  "asignado"
    t.integer  "proveedor_id"
    t.integer  "transporte_id"
    t.string   "track"
    t.datetime "entrada_almacen"
    t.float    "coste_transporte", limit: 24
    t.string   "albaran"
    t.integer  "almacen_id",                   default: 0
  end

  create_table "pedidos", force: true do |t|
    t.string   "pid",                               default: "ES"
    t.integer  "cliente_id"
    t.integer  "taller_id"
    t.integer  "direccion_id"
    t.integer  "taller_nuevo_id"
    t.integer  "neumatico_id"
    t.integer  "status_id"
    t.integer  "envio_id"
    t.integer  "cupon_id"
    t.integer  "test_id"
    t.integer  "entrega_id"
    t.string   "forma_pago"
    t.integer  "descuento"
    t.integer  "descuento_global",                  default: 0
    t.string   "cupon",                 limit: 100
    t.float    "importe",               limit: 24
    t.text     "observaciones_entrega"
    t.string   "estado"
    t.integer  "factura"
    t.string   "usuario_alta"
    t.datetime "fecha_alta"
    t.datetime "fecha_pago"
    t.datetime "fecha_envio"
    t.datetime "fecha_entrega"
    t.string   "fecha_mod"
    t.datetime "fecha_factura"
    t.datetime "entrada_almacen"
    t.string   "usuario_mod"
    t.datetime "fecha_baja"
    t.string   "usuario_baja"
    t.boolean  "manual"
    t.boolean  "aplicar_nfu"
    t.integer  "iva",                   limit: 2,   default: 21
    t.integer  "tarifa",                            default: 0
    t.integer  "rappel",                            default: 0
    t.boolean  "pagado"
    t.boolean  "agrupado"
    t.boolean  "contactado_telefonico"
    t.boolean  "contacto_email"
    t.boolean  "seguimiento"
    t.integer  "afiliacion_id"
    t.boolean  "cancelado"
    t.float    "gastos",                limit: 24,  default: 0.0
    t.boolean  "notificar_cancelar"
    t.datetime "email_date"
    t.text     "comentarios"
  end

  create_table "prefs", force: true do |t|
    t.boolean  "montaje"
    t.boolean  "aleacion"
    t.boolean  "nitrogeno"
    t.float    "precio_hora", limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "presupuesto_items", force: true do |t|
    t.integer  "presupuesto_id"
    t.integer  "quantity"
    t.integer  "item_id"
    t.float    "price",          limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "presupuestos", force: true do |t|
    t.integer  "cliente_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shopping_cart_items", force: true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.integer  "quantity"
    t.integer  "item_id"
    t.string   "item_type"
    t.float    "price",      limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shopping_carts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "montaje"
    t.boolean  "aleacion"
    t.boolean  "nitrogeno"
  end

  create_table "tyres", force: true do |t|
    t.string  "tid",               limit: 22
    t.string  "marca",             limit: 20
    t.string  "modelo",            limit: 50
    t.string  "caracteristica_1",  limit: 20
    t.string  "caracteristica_2",  limit: 20
    t.string  "caracteristica_3",  limit: 20
    t.string  "anchura",           limit: 10
    t.string  "altura",            limit: 10
    t.string  "diametro",          limit: 10
    t.integer "carga"
    t.string  "velocidad",         limit: 2
    t.string  "tipo",              limit: 10
    t.string  "temporada",         limit: 10
    t.integer "origen",                                                default: 0, null: false
    t.boolean "runflat"
    t.integer "entrega"
    t.decimal "precio_origen",                precision: 10, scale: 2
    t.integer "stock"
    t.integer "pvp",                                                   default: 0, null: false
    t.integer "pvp_iva",                                               default: 0, null: false
    t.decimal "pvp_t1",                       precision: 13, scale: 2
    t.decimal "pvp_t1_iva",                   precision: 15, scale: 2
    t.decimal "pvp_t2",                       precision: 13, scale: 2
    t.decimal "pvp_t2_iva",                   precision: 15, scale: 2
    t.decimal "pvp_t3",                       precision: 13, scale: 2
    t.decimal "pvp_t3_iva",                   precision: 15, scale: 2
    t.integer "descuento"
    t.integer "unidades_defecto"
    t.string  "foto",              limit: 50
    t.string  "logo_marca",        limit: 50
    t.decimal "nfu",                          precision: 4,  scale: 2
    t.integer "activo"
    t.boolean "oferta"
    t.boolean "recomendado"
    t.string  "ean",               limit: 50
    t.integer "categoria_peso"
    t.string  "codigo_fabricante"
    t.string  "resistencia"
    t.string  "mojado"
    t.string  "ruido"
  end

  create_table "webs", force: true do |t|
    t.string   "home"
    t.string   "listado"
    t.string   "detalle"
    t.string   "color_primario"
    t.string   "color_secundario"
    t.string   "logo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
