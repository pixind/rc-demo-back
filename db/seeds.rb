# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Prefs.create montaje: true, aleacion: false, nitrogeno: false, precio_hora: 15.00
Web.create home: "default", listado: "default", detalle: "default"
