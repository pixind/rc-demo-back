Rails.application.routes.draw do

  mount DtsCmsAdmin::Engine, at: "cms"

  root 'login#index'

  ##
  # Customers
  resources :clientes do
    member do
      get "pedidos"
    end
  end
  post  'clientes/seleccionar' => "clientes#seleccionar"
  get  'clientes/:id/seleccionar/:proc' => "clientes#seleccionar"

  ##
  # Orders
  resources :pedidos do
    member do
      get "confirmar"
      get "generar"
    end
  end

  ##
  # Shared
  get 'pedidos' => 'static#orders'
  get 'pedidos/:id'  => 'static#order'

  ##
  # Talleres
  get 'taller/inicio' => 'talleres#index'
  get 'taller/pedido_recibido/:id' => 'talleres#pedido_recibido'
  get 'taller/pedido_montado/:id' => 'talleres#pedido_montado'
  get 'taller/preferencias' => 'talleres#preferencias'
  get 'taller/neumaticos' => 'talleres#neumaticos'
  post 'taller/neumaticos' => 'talleres#neumaticos'
  post 'talleres/suplementos'
  get 'taller/:id/neumatico' => 'talleres#neumatico'

  ##
  # Cart
  get  'carrito' => "shopping_carts#show"
  post 'carrito/modificar/:id' => "shopping_carts#modify"
  get  'carrito/eliminar/:id' => "shopping_carts#remove"
  get  'carrito/vaciar' => "shopping_carts#clear"
  get  'carrito/guardar_como_presupuesto' => "shopping_carts#save_as_bugdet"
  get  'carrito/eliminar_presupuesto/:id' => "shopping_carts#destroy_budget"

  ##
  # acts_as_cart routes
  resource :shopping_cart


  ##
  # Proveedores

  # almacen
  get "almacen/inicio"
  get "almacen/pendientes"
  get "almacen/enviados"

  #gerencia
  get "gerencia/inicio"
  get "gerencia/listados"

  #financiero
  get "financiero/inicio"
  get "financiero/distribuidores"
  get "financiero/clientes"
  get "financiero/historico"

  #admin
  get "admin/inicio"
  get "admin/neumaticos"
  get "admin/distribuidores"
  get "admin/talleres"
  get "admin/pedidos"
  get "admin/preferencias"
  get "admin/web"
  get "admin/expedicion_listado"
  get "admin/expedicion_pendientes"
  get "admin/expedicion_enviados"

  #comercial

  get "comercial/neumaticos"
  get 'comercial/:id/neumatico' => 'comercial#neumatico'
  post "comercial/neumaticos"

  ##
  # Login

  get  "login/index"
  post "login/index"

  ##
  # Web Design
  get "web/selector"
  post "web/selector"

end
